;Edmundo Jose Pezantes Urrego
;13 de julio 2020


%macro  escribir 2
    mov eax, 4
    mov ebx, 1
    mov ecx, %1
    mov edx, %2
    int 80h
%endmacro

%macro  leer 2
    mov eax, 3
    mov ebx, 2
    mov ecx, %1
    mov edx, %2
    int 80h
%endmacro

section .data
	msg1		db		10,'-Calculadora-',10,0
	lmsg1		equ		$ - msg1

	msgOpcion		db		10,'--- Opciones ---',0
	lmsgOpcion			equ		$ - msgOpcion

	msgSuma		db		10,'1. Sumar: ',0
	lmsgSuma			equ		$ - msgSuma
 
	msgResta		db		10,'2. Restar:',0
	lmsgResta		equ		$ - msgResta
 
	msgMultiplicacion		db		10,'3. Multiplicar: ',0
	lmsgMultiplicacion		equ		$ - msgMultiplicacion

	msgDivicion		db		10,'4. Dividir: ',0
	lmsgDivicion		equ		$ - msgDivicion

	msgSalir	db		10,'5. Salir: ',0
	lmsgSalir		equ		$ - msgSalir

 
	msg2		db		10,'Numero 1: ',0
	lmsg2		equ		$ - msg2
 
	msg3		db		'Numero 2: ',0
	lmsg3		equ		$ - msg3

	msg9		db		10,'Resultado: ',0
	lmsg9		equ		$ - msg9
 
	msg10		db		10,'Opcion Invalida',10,0
	lmsg10		equ		$ - msg10

	msgSeleccion		db		10,'Establesca su opcion : ',
	lmsgSeleccion		equ		$ - msgSeleccion
 
	nlinea		db		10,10,0
	lnlinea		equ		$ - nlinea
 
section .bss
	opcion:		resb 	2
  	num1:		resb	2
	num2:		resb 	2
	resultado:	resb 	2
 
section .text
 	global _start
 
_start:
	; Imprimimos en pantalla el mensaje de encavezado del ejercicio
    escribir msg1, lmsg1
 
	;**PETICION PARA OBTENER EL PRIMER NUMERO***
    escribir msg2, lmsg2
    leer num1, 2
    ;**PETICION PARA OBTENER EL SEGUNDO NUMERO***
    escribir msg3, lmsg3
    leer num2, 2
 
	;***********mensajes del menu***********
    escribir msgOpcion, lmsgOpcion
	; Imprimimos en pantalla el mensaje de la suma
    escribir msgSuma, lmsgSuma
	mov eax, 4
	; Imprimimos en pantalla el mensaje de la Resta
    escribir msgResta, lmsgResta
	; Imprimimos en pantalla el mensaje de la Multiplicacion
    escribir msgMultiplicacion, lmsgMultiplicacion
	; Imprimimos en pantalla el mensaje de la Divicion
    escribir msgDivicion, lmsgDivicion
	; Imprimimos en pantalla el mensaje de Salir
    escribir msgSalir, lmsgSalir

	;**PETICION PARA OBTENER EL NUMERO DE SELECCION***
menu: 
    escribir msgSeleccion, lmsgSeleccion
    leer opcion, 2

	mov ah,[opcion]
	sub ah,'0'
	;Comparar el valor ingresado por el usuario , para que la operaci
	;JE = jmp (condicional) if exist iqual, salta cuando los valores de dos operandos son iguales, ZF = 1, CF = 0
	cmp ah,1
	JE sumar

	cmp ah,2
	JE restar

	cmp ah,3
	JE multiplicar

	cmp ah,4
	JE dividir

	cmp ah,5
	JE salir
 
	; Si el valor ingresado no cumple con ninguna de las condiciones anteriores entonces mostramos un mensaje de error y finalizamos
	; la ejecucion del programa
	mov eax, 4
	mov ebx, 1
	mov ecx, msg10
	mov edx, lmsg10
	int 80h
	jmp salir
 
 sumar:
	; Movemos los numeros ingresados a los registro AL y BL
	mov al, [num1]
	mov bl, [num2]
 
	; Convertimos los valores ingresados de ascii a decimal
	sub al, '0'
	sub bl, '0'
 
	; Sumamos el registro AL y BL
	add al, bl
;	aaa
 
	; Convertimos el resultado de la suma de decimal a ascii
	add al, '0'
 
	; Movemos el resultado a un espacio reservado en la memoria
	mov [resultado], al
 
	; Imprimimos en pantalla el mensaje 9
	mov eax, 4
	mov ebx, 1
	mov ecx, msg9
	mov edx, lmsg9
	int 80h
 
	; Imprimimos en pantalla el resultado
	mov eax, 4
	mov ebx, 1
	mov ecx, resultado
	mov edx, 2
	int 80h
 
	;jmp restar
	jmp menu
 
restar:
	; Movemos los numeros ingresados a los registro AL y BL
	mov al, [num1]
	mov bl, [num2]
 
	; Convertimos los valores ingresados de ascii a decimal
	sub al, '0'
	sub bl, '0'
 
	; Restamos el registro AL y BL
	sub al, bl
 
	; Convertimos el resultado de la resta de decimal a ascii
	add al, '0'
 
	; Movemos el resultado a un espacio reservado en la memoria
	mov [resultado], al
 
	; Imprimimos en pantalla el mensaje 9
	mov eax, 4
	mov ebx, 1
	mov ecx, msg9
	mov edx, lmsg9
	int 80h
 
	; Imprimimos en pantalla el resultado
	mov eax, 4
	mov ebx, 1
	mov ecx, resultado
	mov edx, 1
	int 80h
 
	;jmp multiplicar
	jmp menu
 
multiplicar:
 
	; Movemos los numeros ingresados a los registro AL y BL
	mov al, [num1]
	mov bl, [num2]
 
	; Convertimos los valores ingresados de ascii a decimal
	sub al, '0'
	sub bl, '0'
 
	; Multiplicamos. AX = AL X BL
	mul bl
 
	; Convertimos el resultado de la resta de decimal a ascii
	add ax, '0'
 
	; Movemos el resultado a un espacio reservado en la memoria
	mov [resultado], ax
 
	; Imprimimos en pantalla el mensaje 9
	mov eax, 4
	mov ebx, 1
	mov ecx, msg9
	mov edx, lmsg9
	int 80h
 
	; Imprimimos en pantalla el resultado
	mov eax, 4
	mov ebx, 1
	mov ecx, resultado
	mov edx, 1
	int 80h
 
	jmp menu

 
dividir:
 
	; Movemos los numeros ingresados a los registro AL y BL
	mov al, [num1]
	mov bl, [num2]
 
	; Igualamos a cero los registros DX y AH
	mov dx, 0
	mov ah, 0
 
	; Convertimos los valores ingresados de ascii a decimal
	sub al, '0'
	sub bl, '0'
 
	; Division. AL = AX / BL. AX = AH:AL
	div bl
 
	; Convertimos el resultado de la resta de decimal a ascii
	add ax, '0'
 
	; Movemos el resultado a un espacio reservado en la memoria
	mov [resultado], ax
 
	; Print on screen the message 9
	mov eax, 4
	mov ebx, 1
	mov ecx, msg9
	mov edx, lmsg9
	int 80h
 
	; Imprimimos en pantalla el resultado
	mov eax, 4
	mov ebx, 1
	mov ecx, resultado
	mov edx, 1
	int 80h
 
	;jmp sumar
	jmp menu
 
salir:
	; Imprimimos en pantalla dos nuevas lineas
	mov eax, 4
	mov ebx, 1
	mov ecx, nlinea
	mov edx, lnlinea
	int 80h
 
	; Finalizamos el programa
	mov eax, 1
	mov ebx, 0
	int 80h
