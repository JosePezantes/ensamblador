; Estudiante: Edmundo Jose Pezantes Urrego
; Curso: 6to "A"
; Fecha: 29 de Julio del 2020

%macro imprimir 2
    mov eax,4
    mov ebx,1
    mov ecx,%1
    mov edx,%2
    int 80h
%endmacro
section .data
    saludo db '*'

    salto db 10
    lensalto equ $-salto

section .txt
    global _start
_start:
    mov ecx,9
    mov ebx,9

l1:
    push ecx
    pop ecx
    loop l2
    jmp salir
l2: 
    imprimir saludo,1
    cmp ecx,0
    jnz l2
    imprimir salto,lensalto
    l1

salir:
    mov eax,1
    int 80h
