; Estudiante: Edmundo Jose Pezantes Urrego
; Curso: 6to "A"
; Fecha: 29 de Julio del 2020

%macro imprimir 2
	mov eax, 4
	mov ebx, 1
	mov ecx, %1
	mov edx, %2
	int 80h
%endmacro
section	.data
	msj db '*'
	new_line db 10,''

section	.text
   global _start        ;must be declared for using gcc
	
_start:	
	
	mov ebx, 9;filas
	mov ecx, 9	;columnas
	jmp principal

principal:
	push ebx
 	cmp ebx, 0    
	jz salir
    jmp ciclo

ciclo:
    push ecx
    imprimir msj,1
    pop ecx
    loop ciclo
salto:
   	imprimir new_line,1 
	pop ebx
	dec ebx
	mov ecx, ebx
	jmp principal

salir:
	mov eax,1
