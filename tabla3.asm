; Estudiante: Edmundo Jose Pezantes Urrego
; Curso: 6to
; Fecha: 5 de Agosto del 2020
section .data
    msj1 db ' * '
    len_msj1 equ $-msj1
    
    msj2 db ' = '
    len_msj2 equ $-msj2
    
    linea db 10
    len equ $-linea

    ;salto db 10
section .bss
	a resb 1
    b resb 1
    c resb 2
section .txt
    global _start
_start:

    mov ax,3
    push ax
    add ax,'0'
    mov [a],ax
    pop ax
    mov cx,1

proceso:
    push cx
    push cx

    add cx,'0'
    mov [b],cx 

    pop cx 
    push ax
    mul cx
    aam
    add al,'0'
    add ah,'0'
    mov [c+0],ah
    mov [c+1],al
    
    
    call pantalla1
    call pantalla2
    call pantalla3
    call pantalla4
    call pantalla5
    call pantalla6

    pop ax;
    pop cx
    inc cx
    cmp cx,9
    jnz proceso
    jmp salir
    
pantalla1: 
    mov eax,4
    mov ebx,1
    mov ecx,a
    mov edx,1
    int 80h
    ret
pantalla2: 
    mov eax,4
    mov ebx,1
    mov ecx,msj1
    mov edx,len_msj1
    int 80h
    ret
pantalla3: 
    mov eax,4
    mov ebx,1
    mov ecx,b
    mov edx,1
    int 80h
    ret
pantalla4: 
    mov eax,4
    mov ebx,1
    mov ecx,msj2
    mov edx,len_msj2
    int 80h
    ret
pantalla5: 
    mov eax,4
    mov ebx,1
    mov ecx,c
    mov edx,2
    int 80h
    ret
pantalla6: 
    mov eax,4
    mov ebx,1
    mov ecx,linea
    mov edx,len
    int 80h
    ret

salir:
    mov eax,1
    int 80h
