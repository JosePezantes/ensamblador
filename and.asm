;Jose Pezantes
	par_msg db 'numero par'
	len1 equ $ - par_msg

	impar_msg db 'numero impar'
	len2 equ $ - impar_msg

setion .text
  global _start
  
  _start:
  	mov ax, 9
  	and ax,1
  	jz par
  	jmp impar
  par:
  	mov eax,4
  	mov ebx, 1
  	mov ecx, par_msg
  	mov ex, len1
  	int 0x80
  	jmp salir
  impar:
  	mov eax, 4
  	mob ebx, 1
  	mov ecx, impar_msg
  	mov edx, len2
  	int 0x80
  	jmp salir
  salir:
  	mov eax,1
  	int 0x80

