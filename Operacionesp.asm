;Edmundo Jose Pezantes Urrego
;Correccion
%macro imprimir 2
    mov eax, 4
    mov ebx, 1
    mov ecx, %1
    mov edx, %2
    int 80H
%endmacro
section .data
    msuma db 'resultado suma :',10
    len_s EQU $ -msuma

    mresta db 10,'resultado resta:',10,13
    len_r EQU $ - mresta

    multi db 'resultado m:',10
    len_m EQU $ - multi

    mcociente db 'resultado cociente:',10
    len_c EQU $ - mcociente
    mresiduo db 'resultado residuo:',10
    len_re EQU $ - mresiduo

    
section .bss
    suma resb 1
    resta resb 1
    multiplicacion resb 1
    cociente resb 1
    residuo resb 1

section .text
    global _start

_start:

    imprimir msuma, len_s
    mov al, 4
    mov bl, 2
    add al, bl
    add al, '0'
    
    mov[suma], al
    imprimir suma,1

    imprimir mresta, len_r

    mov al, 4
    mov bl, 2
    sub al, bl
    add al, '0'
    mov[resta],al
    imprimir resta,1

    imprimir multi, len_m

    mov al, 4
    mov bl, 2
    mul bl
    add al, '0'

    mov[multiplicacion],al

    imprimir multiplicacion,1
    
    imprimir mcociente, len_c

    mov al, 5
    mov bl, 2
    div bl
    add al, '0'
    add ah,'0'
    imprimir cociente, 1
    imprimir mresiduo, len_re
    imprimir residuo, 1

    mov eax,1
    int 80H


    mov [cociente],al
    mov [residuo],ah

    mov eax,4
    mov ebx,1
    mov ecx,cociente
    mov edx,1

    mov eax, 4
    mov ebx, 1
    mov ecx, mresiduo
    mov edx, len_re
    int 80H
