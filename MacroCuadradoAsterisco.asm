;Edmundo Jose pezantes Urrego
;27 de Julio del 2020

%macro imprimir 1
    mov eax,4
    mov ebx,1
    mov ecx,%1
    mov edx,1
    int 80h
%endmacro
section .data
    msj db '*'
    new_line db 10,''
section .text
    global _start
_start:
    mov ebx, 9 ;fila
    mov ecx, 9 ;columna
principal:
    push ebx
    cmp ebx, 0
    jz salir
    jmp asterisco

asterisco:
    dec ecx
    push ecx
    imprimir msj ;el valor de ecx se reemplaza con msj
    pop ecx
    cmp ecx,0
    jg asterisco
    imprimir new_line
    pop ebx
    dec ebx
    mov ecx, 9
    jmp principal

salir:
    mov eax, 1
    int 80h
