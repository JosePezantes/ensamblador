section .data
	mensaje db 'Ingrese un numero ',10
	len_mensaje equ $-mensaje
	
	mensaje2 db 'Ingrese un 2 numero ',10
	len_mensaje2 equ $-mensaje2
	
	mensaje_presentacion db 'El numero ingresado es : ',10
	len_presentacion equ $-mensaje_presentacion
	
section .bss
	a resb 1 
	b resb 1
	resultado resb 1
	

section .text
	global _start
_start:
	;****************************imprime mensaje 1******************
	mov eax,4
	mov ebx, 1
	mov ecx, mensaje
	mov edx, len_mensaje
	int 80h
	;leer primer valor
	mov eax, 3 					;define el tipo de operacion
	mov	ebx, 2					;estandar de entrada
	mov ecx, a		; lo que captura el teclado
	mov edx, 2					; numero de caracteres que se necesitanc on el enter
	int 80h
	
	;****************************imprime mensaje  2******************
	mov eax,4
	mov ebx, 1
	mov ecx, mensaje2
	mov edx, len_mensaje2
	int 80h
	
	;leer segundo valor
	mov eax, 3 					;define el tipo de operacion
	mov	ebx, 2					;estandar de entrada
	mov ecx, b					; lo que captura el teclado
	mov edx, 5					; numero de caracteres
	int 80h
	;****************CONVERTIR VALORES STRING A ENTEROS***************
	mov eax, [a]
	mov ebx, [b]
	sub eax, '0'     
	sub ebx, '0'
	
	add eax, ebx
	add eax, '0'           ; convierto a string
	
	mov [resultado], eax
	
	
	;****************************imprime RESULTADO******************
	mov eax,4
	mov ebx, 1
	mov ecx, mensaje_presentacion
	mov edx, len_presentacion
	int 80h
	
	
	;****************************imprime numero SIEMPRE DEBERA SER DE 32bits******************
	mov eax,4
	mov ebx, 1
	mov ecx, resultado
	mov edx, 1
	int 80h
	;****************************salir del sistema******************
	mov eax,1
	int 80h