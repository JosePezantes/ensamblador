; Estudiante: Edmundo Jose Pezantes Urrego
; Curso: 6to "A"
; Fecha: 29 de Julio del 2020
%macro imprimir 2
     mov eax,4
     mov ebx,1
     mov ecx,%1
     mov edx,%2
     int 80h
%endmacro

section .bss
    contador: resb 1
section .data
    
    msj db '*'

    nlinea		db		10,0
	lnlinea		equ		$ - nlinea

section .text
    global _start    

_start:
    
    mov bx, 9 ;
    mov cx, 9 ;
    
   
principal: ;

    push rbx
    cmp ebx, 0
    jz salir
    jmp asterisco

asterisco:

    ;dec cx
    push rcx
    imprimir msj, 1 ;
    pop rcx 
    loop asterisco

    imprimir nlinea, lnlinea
    pop rbx 
    dec ebx
    mov ecx, ebx ; si se desea formar un cuadrado se debe restablecer el valor a 9 
    jmp principal


salir:
    ;imprimir nlinea, lnlinea

    mov eax, 1
    int 80h
