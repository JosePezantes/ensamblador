; Estudiante: Edmundo Jose Pezantes Urrego
; Curso: 6to "A"
; Fecha: 22 de Julio del 2020
;Cuadrado de asteriscos 

section .data
        asterisco db '*'
        salto db '',10
        lensalto equ $ - salto
section .text
        global _start
_start:

        mov ax,9
        

saltar: 
        mov cx,9

        dec ax
        push ax
        jmp imprimir        

imprimir:
        dec cx
        push cx
        mov eax,4
        mov ebx,1
        mov ecx,asterisco
        mov edx,1
        int 80h
        pop cx
        cmp cx,0
        jnz imprimir


        mov eax,4
        mov ebx,1
        mov ecx,salto
        mov edx,lensalto
        int 80h

        pop ax
        cmp ax,0
        jnz saltar
        

salir:
        mov eax,1
        int 80h
